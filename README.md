# Basic language extension for JastAdd

Syntax highlighting support for https://jastadd.cs.lth.se/

## Features

* Defines a new language `jrag`.
* Add highlighting for `jrag` files. This is an extension of the builtin Java highlighting, so only the jrag-specific keywords are actually contributed by this extension.

## Building

### Prerequisites
- `npm` installed

### Steps
- Run `npm run build` in this folder. This creates a file `jastadd-language-support-N.N.N.vsix`.

## Installing a vsix file

- Open VS Code
- Open the extension tab
- In the top right of the extension tab there is a three-dot "..." menu. Click it, and choose "Install from VSIX..".
- Reload the window when prompted, and then you should see keywords highlighted in jrag files.
